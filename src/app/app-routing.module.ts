import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';

const routes: Routes = [
  { path:'', component: HomePageComponent},
  { path: 'departments', loadChildren: () => import('./modules/departments/departments.module').then(m => m.DepartmentsModule) },
  { path: 'prefecture', loadChildren: () => import('./modules/prefecture/prefecture.module').then(m => m.PrefectureModule) }, 
  { path: 'regions', loadChildren: () => import('./modules/regions/regions.module').then(m => m.RegionsModule) }, 
  { path: 'usaStates', loadChildren: () => import('./modules/usa-states/usa-states.module').then(m => m.UsaStatesModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
