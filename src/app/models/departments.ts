export interface Departments {
	number: string,
	name: string,
	prefecture: string,
	region: string
}
