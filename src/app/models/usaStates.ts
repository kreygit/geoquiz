export interface UsaStates {
	id: string,
	name: string,
	capital: string
}