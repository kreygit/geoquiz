import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-map-usa-states',
  templateUrl: './map-usa-states.component.html',
  styleUrls: ['./map-usa-states.component.scss']
})
export class MapUsaStatesComponent implements OnInit {

  @Output() 
  newItemEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    const paths = document.querySelectorAll("path");
    // Listen each click evenement on path element
    paths.forEach(path => {
      path?.addEventListener("click", () => {
        this.statesSelected(path);
      });
    })
  }

  statesSelected(path: any){
    this.newItemEvent.emit(path.id);
  }
}
