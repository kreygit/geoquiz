import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapUsaStatesComponent } from './map-usa-states.component';

describe('MapUsaStatesComponent', () => {
  let component: MapUsaStatesComponent;
  let fixture: ComponentFixture<MapUsaStatesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapUsaStatesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapUsaStatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
