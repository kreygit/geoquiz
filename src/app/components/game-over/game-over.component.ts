import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-game-over',
  templateUrl: './game-over.component.html',
  styleUrls: ['./game-over.component.scss']
})
export class GameOverComponent implements OnInit {

  winner: boolean = true;

  @Output() 
  startNewGame = new EventEmitter<string>();
  
  @Input()
  numberOfLife!: number;
  

  constructor() { }

  ngOnInit(): void {
    if (this.numberOfLife === 0) {
      this.winner = false;
    }
  }

  startNewGameFunction() {
    this.startNewGame.emit();
  }
}
