import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-life-counter',
  templateUrl: './life-counter.component.html',
  styleUrls: ['./life-counter.component.scss']
})
export class LifeCounterComponent implements OnInit {

  @Input()
  lives!: number;

  constructor() { }

  ngOnInit(): void {
  }

  lifeSequence(n: number): Array<number> {
    return Array(n);
  }
}
