import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodAnswerComponent } from './good-answer.component';

describe('GoodAnswerComponent', () => {
  let component: GoodAnswerComponent;
  let fixture: ComponentFixture<GoodAnswerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoodAnswerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodAnswerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
