import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapFranceDepartmentComponent } from './map-france-department.component';

describe('MapFranceDepartmentComponent', () => {
  let component: MapFranceDepartmentComponent;
  let fixture: ComponentFixture<MapFranceDepartmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapFranceDepartmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapFranceDepartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
