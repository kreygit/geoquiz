import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-map-france-department',
  templateUrl: './map-france-department.component.html',
  styleUrls: ['./map-france-department.component.scss']
})
export class MapFranceDepartmentComponent implements OnInit {

  @Output() 
  newItemEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    const paths = document.querySelectorAll("path");
    // Listen each click evenement on path element
    paths.forEach(path => {
      path?.addEventListener("click", () => {
        this.departmentSelected(path);
      });
    })
  }

  departmentSelected(path: any){
    this.newItemEvent.emit(path.id);
  }
}
