import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-map-france-region',
  templateUrl: './map-france-region.component.html',
  styleUrls: ['./map-france-region.component.scss']
})
export class MapFranceRegionComponent implements OnInit {

  @Output() 
  newItemEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    const paths = document.querySelectorAll("path");
    // Listen each click evenement on path element
    paths.forEach(path => {
      path?.addEventListener("click", () => {
        this.regionSelected(path);
      });
    })
  }

  regionSelected(path: any){
    this.newItemEvent.emit(path.id);
  }
}
