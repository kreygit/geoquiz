import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapFranceRegionComponent } from './map-france-region.component';

describe('MapFranceRegionComponent', () => {
  let component: MapFranceRegionComponent;
  let fixture: ComponentFixture<MapFranceRegionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapFranceRegionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapFranceRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
