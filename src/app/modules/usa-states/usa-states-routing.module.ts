import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsaStatesComponent } from './usa-states.component';

const routes: Routes = [{ path: '', component: UsaStatesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsaStatesRoutingModule { }
