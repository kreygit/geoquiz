import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsaStatesComponent } from './usa-states.component';

describe('UsaStatesComponent', () => {
  let component: UsaStatesComponent;
  let fixture: ComponentFixture<UsaStatesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsaStatesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsaStatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
