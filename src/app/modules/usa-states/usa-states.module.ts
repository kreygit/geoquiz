import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsaStatesRoutingModule } from './usa-states-routing.module';
import { UsaStatesComponent } from './usa-states.component';
import { MaterialModule } from '../../material.module';
import { CommonComponentsModule } from '../../common-components.module';


@NgModule({
  declarations: [UsaStatesComponent],
  imports: [
    CommonModule,
    UsaStatesRoutingModule,
    MaterialModule,
    CommonComponentsModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class UsaStatesModule { }
