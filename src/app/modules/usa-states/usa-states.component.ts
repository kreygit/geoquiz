import { Component, OnInit } from '@angular/core';
import { JsonService } from 'src/app/services/json.service';
import { PlayerService } from 'src/app/services/player.service';

@Component({
  selector: 'app-usa-states',
  templateUrl: './usa-states.component.html',
  styleUrls: ['./usa-states.component.scss']
})
export class UsaStatesComponent implements OnInit {

  constructor(
    private jsonService: JsonService,
    public player: PlayerService,
  ) { }

  ngOnInit(): void {
    this.jsonService.getUsaJsonFile()
    .subscribe((data: any) => {
      this.player.values = data;
      // Get 10 random departments from this.departments array
      this.player.valuesSelected = this.player.values.sort(() => Math.random() - Math.random()).slice(0, 10);
      this.player.valueToFind = this.player.valuesSelected[0];
    })
  }
}
