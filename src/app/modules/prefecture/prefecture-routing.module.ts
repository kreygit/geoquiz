import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrefectureComponent } from './prefecture.component';

const routes: Routes = [{ path: '', component: PrefectureComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrefectureRoutingModule { }
