import { CUSTOM_ELEMENTS_SCHEMA,NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrefectureRoutingModule } from './prefecture-routing.module';
import { PrefectureComponent } from './prefecture.component';
import { MaterialModule } from '../../material.module';
import { CommonComponentsModule } from '../../common-components.module';


@NgModule({
  declarations: [
    PrefectureComponent,

  ],
  imports: [
    CommonModule,
    PrefectureRoutingModule,
    MaterialModule,
    CommonComponentsModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class PrefectureModule { }
