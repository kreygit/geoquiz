import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DepartmentsRoutingModule } from './departments-routing.module';
import { MaterialModule } from '../../material.module';
import { CommonComponentsModule } from '../../common-components.module';
import { DepartmentsComponent } from './departments.component';



@NgModule({
  declarations: [
    DepartmentsComponent,
  ],
  imports: [
    CommonModule,
    DepartmentsRoutingModule,
    MaterialModule,
    CommonComponentsModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class DepartmentsModule { }
