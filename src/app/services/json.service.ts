import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class JsonService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  getDepartmentJsonFile() {
    return this.httpClient.get('../../assets/france.json');
  }

  getRegionJsonFile() {
    return this.httpClient.get('../../assets/france-region.json');
  }

  getUsaJsonFile() {
    return this.httpClient.get('../../assets/usa.json');
  }
}
