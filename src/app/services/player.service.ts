import { Injectable } from '@angular/core';
import { Values } from '../models/values';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  values: Values[] = [];
  valuesSelected: Values[] = [];
  valueToFind: any = '';
  valuesSelectedLength: number = 0;

  lives: number = 3;
  counter: number = 1;
  goodAnswer: boolean = false;
  badAnswer: boolean = false;
  gameOver: boolean = false;

  constructor() { 
    
  }

  getAnswer(id: any){
    if (this.lives){
      if (id === this.valueToFind.id) {
        this.goodAnswerFunction();
      } else {
        this.badAnswerFunction();
      }
    }
  }

  goodAnswerFunction(){
    this.valuesSelectedLength = this.counter === 1 ? this.valuesSelected.length : this.valuesSelectedLength;
    if (this.counter !== this.valuesSelectedLength) {
      this.valuesSelected.shift();
      this.valueToFind = this.valuesSelected[0];
      this.counter += 1;
      this.goodAnswer = true;
      setTimeout(() => {
        this.goodAnswer = false;
      }, 2000);
    } else {
      this.gameOver = true;
    }
  }

  badAnswerFunction(){
    this.lives -= 1;
    if (this.lives !== 0) {
      this.badAnswer = true;
      setTimeout(() => {
        this.badAnswer = false;
      }, 2000);
    } else {
      this.gameOver = true;
      this.showAnswer();
    }
  }

  showAnswer() {
    const paths = document.querySelectorAll("path");
    paths.forEach((path:any) => {
      if (path.id === this.valueToFind.id) {
        path.classList.add('game-over');
      }
    })

  }

  startNewGame(parent: any){
    this.lives = 3;
    this.counter = 1;
    this.goodAnswer = false;
    this.badAnswer = false;
    this.gameOver = false;
    const paths = document.querySelectorAll("path");
    paths.forEach((path: any) => {
      path.classList.remove('game-over');
    })
    parent.ngOnInit();
  }

}
