import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LifeCounterComponent } from './components/life-counter/life-counter.component';
import { MapFranceDepartmentComponent } from './components/map-france-department/map-france-department.component';
import { MapFranceRegionComponent } from './components/map-france-region/map-france-region.component';
import { GoodAnswerComponent } from './components/good-answer/good-answer.component';
import { BadAnswerComponent } from './components/bad-answer/bad-answer.component';
import { GameOverComponent } from './components/game-over/game-over.component';
import { MaterialModule } from './material.module';
import { MapUsaStatesComponent } from './components/map-usa-states/map-usa-states.component';


@NgModule({
  declarations: [
    LifeCounterComponent,
    MapFranceDepartmentComponent,
    MapFranceRegionComponent,
    GoodAnswerComponent,
    BadAnswerComponent,
    GameOverComponent,
    MapUsaStatesComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
  ],
  exports: [
    CommonModule,
    LifeCounterComponent,
    MapFranceDepartmentComponent,
    MapFranceRegionComponent,
    GoodAnswerComponent,
    BadAnswerComponent,
    GameOverComponent,
    MapUsaStatesComponent,
  ]
})
export class CommonComponentsModule { }
